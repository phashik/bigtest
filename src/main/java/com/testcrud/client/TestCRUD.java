package com.testcrud.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class TestCRUD implements EntryPoint {
    private ArrayList<User> users = new ArrayList<User>();

    private VerticalPanel pnlMain = new VerticalPanel();
    private HorizontalPanel pnlTop = new HorizontalPanel();
    private HorizontalPanel pnlLabels = new HorizontalPanel();
    private FlexTable tblDisplay = new FlexTable();

    private Button btnCreate = new Button("Create");
    private Button btnFind = new Button("Find");
    private Button btnRefresh = new Button("Refresh");
    private Button btnSelect = new Button("Select");
    private Button btnUpdate = new Button("Update");

    private Label lblSize = new Label("Size = 0");

    private IntegerBox tbId = new IntegerBox();
    private TextBox tbName = new TextBox();
    private IntegerBox tbAge = new IntegerBox();

    private CRUDServiceAsync CRUDsvc = GWT.create(CRUDService.class);

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        //todo: load from database

        initButtonHandlers();
        initTableDisplay();
        initPanelTop();
        initPanelLabels();

        pnlMain.add(pnlLabels);
        pnlMain.add(pnlTop);
        pnlMain.add(lblSize);
        pnlMain.add(tblDisplay);

        RootPanel.get("WIDGETHERE").add(pnlMain);
        refreshDisplay();
    }

    private void initButtonHandlers() {
        btnCreate.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                createNewUser();
            }
        });
        btnUpdate.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                updateUser();
            }
        });
        tbAge.addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    createNewUser();
                }
            }
        });
        tbName.addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    createNewUser();
                }
            }
        });
    }

    private void initPanelTop() {
        tbId.setReadOnly(true);
        pnlTop.add(tbId);
        pnlTop.add(tbName);
        pnlTop.add(tbAge);
        pnlTop.add(btnCreate);
        pnlTop.add(btnUpdate);
        tbId.addStyleName("displayColumn");
        tbName.addStyleName("displayColumn");
        tbAge.addStyleName("displayColumn");
        btnCreate.addStyleName("displayColumn");
        btnUpdate.addStyleName("displayColumn");
    }

    private void initPanelLabels() {
        Label tmp = new Label("ID");
        tmp.addStyleName("displayColumn");
        pnlLabels.add(tmp);
        tmp = new Label("Name");
        tmp.addStyleName("displayColumn");
        pnlLabels.add(tmp);
        tmp = new Label("Age");
        tmp.addStyleName("displayColumn");
        pnlLabels.add(tmp);
        tmp = new Label("");
        tmp.addStyleName("displayColumn");
        pnlLabels.add(tmp);
        tmp = new Label("");
        tmp.addStyleName("displayColumn");
        pnlLabels.add(tmp);
    }
    
    private void initTableDisplay() {
        pnlMain.remove(tblDisplay);
        tblDisplay = new FlexTable();
        pnlMain.add(tblDisplay);
        tblDisplay.setText(0, 0, "Id");
        tblDisplay.setText(0, 1, "Name");
        tblDisplay.setText(0, 2, "Age");
        tblDisplay.setText(0, 3, "Select");
        tblDisplay.setText(0, 4, "Delete");
        // Add styles to elements in the stock list table.
        tblDisplay.getRowFormatter().addStyleName(0, "displayHeader");
        tblDisplay.getColumnFormatter().addStyleName(0, "displayColumn");
        tblDisplay.getColumnFormatter().addStyleName(1, "displayColumn");
        tblDisplay.getColumnFormatter().addStyleName(2, "displayColumn");
        tblDisplay.getColumnFormatter().addStyleName(3, "displayColumn");
        tblDisplay.getColumnFormatter().addStyleName(4, "displayColumn");
    }

    private void createNewUser() {
        String name = tbName.getText();
        Integer age = tbAge.getValue();

        AsyncCallback<java.lang.Void> callback = new AsyncCallback<java.lang.Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //todo: do something
            }

            @Override
            public void onSuccess(java.lang.Void t) {
                refreshDisplay();
                clearTextBoxes();
            }
        };

        CRUDsvc.createNewUser(name, age, callback);
    }

    private void deleteUser(int id) {

        AsyncCallback<java.lang.Void> callback = new AsyncCallback<java.lang.Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //todo: do something
            }

            @Override
            public void onSuccess(java.lang.Void t) {
                refreshDisplay();
            }
        };

        CRUDsvc.deleteUser(id, callback);
    }

    private void updateUser() {
        Integer id = tbId.getValue();
        String name = tbName.getText();
        Integer age = tbAge.getValue();

        AsyncCallback<java.lang.Void> callback = new AsyncCallback<java.lang.Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //todo: do something
            }

            @Override
            public void onSuccess(java.lang.Void t) {
                refreshDisplay();
                clearTextBoxes();
            }
        };

        CRUDsvc.updateUser(id, name, age, callback);
    }

    private void clearTextBoxes() {
        tbId.setText("");
        tbName.setText("");
        tbAge.setText("");
    }

    private void refreshDisplay() {
        AsyncCallback<ArrayList<User>> callback = new AsyncCallback<ArrayList<User>>() {
            @Override
            public void onFailure(Throwable caught) {
                //todo: do something
            }

            @Override
            public void onSuccess(ArrayList<User> list) {
                users = list;
                initTableDisplay();
                int row = 1;
                for (final User user : users) {
                    if (!user.isRemoved()) {
                        tblDisplay.setText(row, 0, String.valueOf(user.getId()));
                        tblDisplay.setText(row, 1, user.getName());
                        tblDisplay.setText(row, 2, String.valueOf(user.getAge()));
                        Button select = new Button("select");
                        select.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                tbId.setText(String.valueOf(user.getId()));
                                tbName.setText(user.getName());
                                tbAge.setText(String.valueOf(user.getAge()));
                            }
                        });
                        Button remove = new Button("x");
                        remove.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                deleteUser(user.getId());
                            }
                        });
                        tblDisplay.setWidget(row, 3, select);
                        tblDisplay.setWidget(row, 4, remove);
                        if (row % 2 == 0)
                            tblDisplay.getRowFormatter().addStyleName(row, "grayRow");
                        row++;
                    }
                }
                lblSize.setText("Size = " + users.size());
            }
        };

        CRUDsvc.readUsers(callback);
    }
}
