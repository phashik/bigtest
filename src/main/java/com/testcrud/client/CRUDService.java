package com.testcrud.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

@RemoteServiceRelativePath("springGwtServices/greetingService")
public interface CRUDService extends RemoteService {
    void createNewUser(String name, int age);
    void updateUser(int id, String name, int age);
    void deleteUser(int id);
    ArrayList<User> readUsers ();
}
