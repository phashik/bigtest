package com.testcrud.client;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String name;
    private int age;
    private boolean removed = false;

    public User () {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isRemoved () {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public void remove() {
        this.removed = true;
    }
}
