package com.testcrud.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

public interface CRUDServiceAsync {
    void createNewUser(String name, int age, AsyncCallback<Void> async);

    void updateUser(int id, String name, int age, AsyncCallback<Void> async);

    void deleteUser(int id, AsyncCallback<Void> async);

    void readUsers(AsyncCallback<ArrayList<User>> async);
}
