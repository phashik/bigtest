package com.testcrud.server;

import com.testcrud.client.CRUDService;
import com.testcrud.client.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("greetingService")
public class CRUDServiceImpl implements CRUDService {
    private dataBridge ME = new dataBridge();

    @Override
    public void createNewUser(String name, int age) {
        ME.addUser(name, age);
    }

    public void updateUser(int id, String name, int age) {
        ME.updateUser(id, name, age);
    }

    public void deleteUser(int id) {
        ME.deleteUser(id);
    }

    public ArrayList<User> readUsers () {
        return ME.listUsers();
    }
}